package com.epam.courses.controller;

import com.epam.courses.model.task1.PingPong;
import com.epam.courses.model.task2.Fibonacci;
import com.epam.courses.model.task5.SleepingTask;
import com.epam.courses.model.task6.MultiSynchronizedThreeMethods;
import com.epam.courses.model.task6.SingleSynchronizedThreeMethods;
import com.epam.courses.model.task7.Consumer;
import com.epam.courses.model.task7.Producer;
import java.io.PipedOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainViewController {

  private static Logger log = LogManager.getLogger(MainViewController.class);
  private static final int FIRST_TASK_TIME_SECONDS = 1;
  private static Random random = new Random();

  PingPong pingPong;

  public MainViewController() {
    pingPong = new PingPong();
  }

  public void runFirstTaskTest() {
    pingPong.startPingPong();
    try {
      TimeUnit.SECONDS.sleep(FIRST_TASK_TIME_SECONDS);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    pingPong.setFlag(false);
  }

  public void runSecondTaskTest() {
    List<Fibonacci> fibonacciList = new ArrayList<>();
    final int countOfThreads = 6;
    for (int i = 0; i < countOfThreads; i++) {
      fibonacciList.add(new Fibonacci(random.nextInt(10) + 20));
    }
    List<Thread> threadList = new ArrayList<>();
    for (int i = 0; i < countOfThreads; i++) {
      threadList.add(new Thread(fibonacciList.get(i)));
      threadList.get(i).start();
      try {
        threadList.get(i).join();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

    }
    for (int i = 0; i < countOfThreads; i++) {
      log.info(Arrays.toString(fibonacciList.get(i).getFibonacciSequence()));
    }
  }

  public void runThirdTaskTest() {
    com.epam.courses.model.task3.Fibonacci fibonacci1 = new com.epam.courses.model.task3.Fibonacci(
        random.nextInt(10) + 20);
    com.epam.courses.model.task3.Fibonacci fibonacci2 = new com.epam.courses.model.task3.Fibonacci(
        random.nextInt(10) + 20);
    com.epam.courses.model.task3.Fibonacci fibonacci3 = new com.epam.courses.model.task3.Fibonacci(
        random.nextInt(10) + 20);
    com.epam.courses.model.task3.Fibonacci fibonacci4 = new com.epam.courses.model.task3.Fibonacci(
        random.nextInt(10) + 20);
    com.epam.courses.model.task3.Fibonacci fibonacci5 = new com.epam.courses.model.task3.Fibonacci(
        random.nextInt(10) + 20);
    com.epam.courses.model.task3.Fibonacci fibonacci6 = new com.epam.courses.model.task3.Fibonacci(
        random.nextInt(10) + 20);
    com.epam.courses.model.task3.Fibonacci fibonacci7 = new com.epam.courses.model.task3.Fibonacci(
        random.nextInt(10) + 20);
    com.epam.courses.model.task3.Fibonacci fibonacci8 = new com.epam.courses.model.task3.Fibonacci(
        random.nextInt(10) + 20);
    com.epam.courses.model.task3.Fibonacci fibonacci9 = new com.epam.courses.model.task3.Fibonacci(
        random.nextInt(10) + 20);
    ExecutorService fixedExecutorService = Executors.newFixedThreadPool(2);
    fixedExecutorService.execute(fibonacci1);
    fixedExecutorService.execute(fibonacci2);
    fixedExecutorService.execute(fibonacci3);
    fixedExecutorService.shutdown();

    ExecutorService singleExecutorService = Executors.newSingleThreadExecutor();
    singleExecutorService.execute(fibonacci4);
    singleExecutorService.execute(fibonacci5);
    singleExecutorService.execute(fibonacci6);
    singleExecutorService.shutdown();

    ExecutorService cachedExecutorService = Executors.newCachedThreadPool();
    cachedExecutorService.execute(fibonacci7);
    cachedExecutorService.execute(fibonacci8);
    cachedExecutorService.execute(fibonacci9);
    cachedExecutorService.shutdown();
  }

  public void runFourthTaskTest() {
    ExecutorService cachedExecutor = Executors.newFixedThreadPool(3);
    ArrayList<Future<List<Integer>>> fibonacciSequences = new ArrayList<>();

    for (int i = 0; i < 20; i++) {
      fibonacciSequences.add(cachedExecutor
          .submit(new com.epam.courses.model.task4.Fibonacci(random.nextInt(10) + i)));
    }
    Thread.yield();
    cachedExecutor.shutdown();

    for (Future<List<Integer>> fibonacciSequence : fibonacciSequences) {
      try {
        fibonacciSequence.get().forEach(sequenceNumber -> System.out.print(sequenceNumber + " "));
        System.out.println();
      } catch (ExecutionException | InterruptedException e) {
        e.printStackTrace();
      } finally {
        cachedExecutor.shutdown();
      }
    }
  }

  public void runFifthTaskTest() {
    ExecutorService cachedExecutorService = Executors.newCachedThreadPool();
    Scanner scanner = new Scanner(System.in);
    log.info("Please enter quantity of tasks: ");
    int quantity = scanner.nextInt();
    for (int i = 0; i < quantity; i++) {
      cachedExecutorService.submit(new SleepingTask());
    }
    cachedExecutorService.shutdown();
  }

  public void runSixthTaskTest() {
    MultiSynchronizedThreeMethods threeMethods = new MultiSynchronizedThreeMethods();
    new Thread(threeMethods::firstMethod).start();
    new Thread(threeMethods::secondMethod).start();
    new Thread(threeMethods::thirdMethod).start();

    SingleSynchronizedThreeMethods singleSynchronizedThreeMethods = new SingleSynchronizedThreeMethods();
    Thread testThread1 = new Thread(singleSynchronizedThreeMethods::firstMethod);
    testThread1.start();
    Thread testThread2 = new Thread(singleSynchronizedThreeMethods::secondMethod);
    testThread2.start();
    Thread testThread3 = new Thread(singleSynchronizedThreeMethods::thirdMethod);
    testThread3.start();
    try {
      testThread1.join();
      testThread2.join();
      testThread3.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void runSeventhTaskTest() {
    Producer producer = new Producer();
    PipedOutputStream outputStream = producer.getOutputStream();

    Consumer consumer = new Consumer(outputStream);

    Thread thread1 = new Thread(producer);
    Thread thread2 = new Thread(consumer);

    thread1.start();
    thread2.start();
    try {
      thread1.join();
      thread2.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
