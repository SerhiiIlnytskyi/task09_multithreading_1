package com.epam.courses.model.task7;

import java.io.IOException;
import java.io.PipedOutputStream;

public class Producer implements Runnable{

  private final PipedOutputStream outputStream;

  public Producer() {
    outputStream = new PipedOutputStream();
  }

  @Override
  public void run() {
    try {
      outputStream.write("Hello from Producer".getBytes());
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        outputStream.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public PipedOutputStream getOutputStream() {
    return outputStream;
  }
}
