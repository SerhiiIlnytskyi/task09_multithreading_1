package com.epam.courses.model.task7;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class Consumer implements Runnable {

  private PipedInputStream inputStream;

  public Consumer(PipedOutputStream pipedOutputStream){
    try {
      this.inputStream = new PipedInputStream(pipedOutputStream);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void run() {
    try {
      while (true) {
        int byteData = inputStream.read();
        if (byteData != -1) {
          System.out.print((char) byteData);
        } else {
          System.out.println();
          return;
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        inputStream.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }
}
