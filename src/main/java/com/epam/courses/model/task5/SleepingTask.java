package com.epam.courses.model.task5;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SleepingTask implements Callable<Integer> {

  private static Logger log = LogManager.getLogger(SleepingTask.class);

  private static Random random = new Random();
  private final int sleepTime;

  public SleepingTask() {
    sleepTime = random.nextInt(10) + 1;
  }

  @Override
  public Integer call() {
    try {
      TimeUnit.SECONDS.sleep(sleepTime);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    log.info(this + " task seeped - " + sleepTime + " seconds");
    return sleepTime;
  }
}
