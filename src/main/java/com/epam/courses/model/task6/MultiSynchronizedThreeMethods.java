package com.epam.courses.model.task6;

import com.epam.courses.model.task5.SleepingTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MultiSynchronizedThreeMethods {

  private static Logger log = LogManager.getLogger(MultiSynchronizedThreeMethods.class);
  private final Object firstSync = new Object();
  private final Object secondSync = new Object();
  private final Object thirdSync = new Object();

  public void firstMethod() {
    synchronized (firstSync) {
      for (int i = 0; i < 15; i++) {
        log.info("Another sync object: first Method message");
        Thread.yield();
      }
    }
  }

  public void secondMethod() {
    synchronized (secondSync) {
      for (int i = 0; i < 15; i++) {
        log.info("Another sync object: second Method message");
        Thread.yield();
      }
    }
  }

  public void thirdMethod() {
    synchronized (thirdSync) {
      for (int i = 0; i < 15; i++) {
        log.info("Another sync object: third Method message");
        Thread.yield();
      }
    }
  }
}
