package com.epam.courses.model.task6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SingleSynchronizedThreeMethods {

  private static Logger log = LogManager.getLogger(SingleSynchronizedThreeMethods.class);
  private final Object sync = new Object();

  public void firstMethod() {
    synchronized (sync) {
      for (int i = 0; i < 15; i++) {
        log.info("Same sync object: first Method message");
        Thread.yield();
      }
    }
  }

  public void secondMethod() {
    synchronized (sync) {
      for (int i = 0; i < 15; i++) {
        log.info("Same sync object: second Method message");
        Thread.yield();
      }
    }
  }

  public void thirdMethod() {
    synchronized (sync) {
      for (int i = 0; i < 15; i++) {
        log.info("Same sync object: third Method message");
        Thread.yield();
      }
    }
  }
}
