package com.epam.courses.model.task4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class Fibonacci implements Callable<List<Integer>> {

  private int count;
  private List<Integer> fibonacciSequence;

  public Fibonacci(int count) {
    this.count = count;
    fibonacciSequence = new ArrayList<>();
  }

  private int calculateSequence(int number) {
    if (number < 2) {
      return 1;
    }
    return calculateSequence(number - 2) + calculateSequence(number - 1);
  }

  @Override
  public List<Integer> call() throws Exception {
    for (int i = 0; i < count; i++) {
      fibonacciSequence.add(calculateSequence(i));
    }
    return fibonacciSequence;
  }
}
