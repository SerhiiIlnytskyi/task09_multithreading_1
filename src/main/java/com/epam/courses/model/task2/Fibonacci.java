package com.epam.courses.model.task2;

public class Fibonacci implements Runnable {

  private int count;
  private int[] fibonacciSequence;

  public Fibonacci(int count) {
    this.count = count;
    fibonacciSequence = new int[count];
  }

  private int calculateSequence(int number) {
    if (number < 2) {
      return 1;
    }
    return calculateSequence(number - 2) + calculateSequence(number - 1);
  }

  @Override
  public void run() {
    for (int i = 0; i < count; i++) {
      fibonacciSequence[i] = calculateSequence(i);
    }
  }

  public int[] getFibonacciSequence() {
    return fibonacciSequence;
  }
}
