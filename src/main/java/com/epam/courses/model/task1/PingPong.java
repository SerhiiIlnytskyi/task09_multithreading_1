package com.epam.courses.model.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPong {

  private static Logger log = LogManager.getLogger(PingPong.class);
  private static final Object sync = new Object();
  private volatile boolean flag = true;

  public void startPingPong() {

    Thread pingThread = new Thread(
        () -> {
          synchronized (sync) {
            while (flag) {
              try {
                sync.wait();
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              log.info("ping");
              sync.notify();
            }
          }
        }
    );
    Thread pongThread = new Thread(
        () -> {
          synchronized (sync) {
            while (flag) {
              sync.notify();
              try {
                sync.wait();
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              log.info("pong");
            }
          }
        }
    );
    pingThread.start();
    pongThread.start();
  }

  public void setFlag(boolean flag) {
    this.flag = flag;
  }
}
